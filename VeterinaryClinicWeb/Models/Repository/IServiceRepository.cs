﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VeterinaryClinicWeb.Models.Repository
{
    interface IServiceRepository
    {
        IQueryable<Service> Services { get; }

        Service GetServiceById(int serviceId);
        bool CreateService(Service service);
        bool UpdateService(Service service);
        bool RemoveService(Service service);
    }
}
