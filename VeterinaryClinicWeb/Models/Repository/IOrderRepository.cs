﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VeterinaryClinicWeb.Models.Repository
{
    interface IOrderRepository
    {
        IQueryable<Order> Orders { get; }

        Order GetOrderById(int orderId);
        bool CreateOrder(Order order);
        bool UpdateOrder(Order order);
        bool RemoveOrder(Order order);
    }
}
