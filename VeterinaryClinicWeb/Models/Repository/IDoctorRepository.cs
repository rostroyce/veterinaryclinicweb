﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VeterinaryClinicWeb.Models.Repository
{
    interface IDoctorRepository
    {
        IQueryable<Doctor> Doctors { get; }

        Doctor GetDoctorById(int doctorId);
        bool CreateDoctor(Doctor doctor);
        bool UpdateDoctor(Doctor doctor);
        bool RemoveDoctor(Doctor doctor);
    }
}
