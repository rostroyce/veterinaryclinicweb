﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VeterinaryClinicWeb.Helpers;

namespace VeterinaryClinicWeb.Models
{
    public class Service
    {
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(70)]
        [Column(TypeName = "varchar(70)")]
        public string ServiceName { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Column(TypeName="varchar(250)")]
        [MaxLength(250)]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "varchar(25)")]
        public string serviceCategory { get; set; }
    }
}
