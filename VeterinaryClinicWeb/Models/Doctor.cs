using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VeterinaryClinicWeb.Models
{
    public class Doctor
    {
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(70)]
        [Column(TypeName = "varchar(70)")]
        public string DoctorName { get; set; }

        [Required]
        [MaxLength(40)]
        [Column(TypeName = "varchar(40)")]
        public string Proffesion { get; set; }

        [Required]
        [MaxLength(300)]
        [Column(TypeName = "varchar(300)")]
        public string Profile { get; set; }

        [Column(TypeName = "varchar(150)")]
        [MaxLength(150)]
        public string ImagePath { get; set; }
    }
}