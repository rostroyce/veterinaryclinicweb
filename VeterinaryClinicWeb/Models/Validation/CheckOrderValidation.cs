using System.ComponentModel.DataAnnotations;

namespace VeterinaryClinicWeb.Models.Validation
{
    public class CheckOrderValidation
    {
        [Required(ErrorMessage = "There are no order with number you input.")]
        public int OrderNumber { get; set; }

        [Required(ErrorMessage = "Please enter last 4 digits of your phone number.")]
        [MinLength(4)]
        [MaxLength(4)]
        public string PhoneDigits { get; set; }

        public override string ToString()
        {
            return "OrderNumber: " + OrderNumber + ";  PhoneDigits: " + PhoneDigits + ";";
        }
    }

}