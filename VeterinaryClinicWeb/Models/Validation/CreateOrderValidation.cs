using System.ComponentModel.DataAnnotations;

namespace VeterinaryClinicWeb.Models.Validation
{
    public class CreateOrderValidation
    {
        [Required(ErrorMessage = "Enter your name and surname (min 6 symbols, max 70 symbols).")]
        [MinLength(6)]
        [MaxLength(70)]
        public string Customer { get; set; }

        [Required(ErrorMessage = "Enter description (max 500 symbols).")]
        [MaxLength(500)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Enter your phone (max 20 symbols).")]
        [MaxLength(20)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Enter your email (max 80 symbols).")]
        [MaxLength(80)]
        public string Email { get; set; }
    }

}