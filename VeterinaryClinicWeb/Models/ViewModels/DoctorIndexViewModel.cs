using System;
using System.Collections.Generic;

namespace VeterinaryClinicWeb.Models.ViewModels
{
    public class DoctorIndexViewModel
    {
        public IEnumerable<Doctor> Doctors { get; set; }
        public PageViewModel Pagination { get; set; }
    }
}