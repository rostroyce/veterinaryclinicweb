using System;
using System.Collections.Generic;

namespace VeterinaryClinicWeb.Models.ViewModels
{
    public class CheckOrderViewModel
    {
        public bool HavePermission { get; set; } = false;
        public Order order { get; set; } 
    }
}