using System;

namespace VeterinaryClinicWeb.Models.ViewModels
{
    public class PageViewModel
    {
        public int PageNumber {get; private set;}
        public int TotalPages {get; private set;}

        public PageViewModel(int count, int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber;
            this.TotalPages = (int)Math.Ceiling(count/ (double)pageSize);
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageNumber > 1);
            }
        }
 
        public bool HasNextPage
        {
            get
            {
                return (PageNumber < TotalPages);
            }
        }
    }
}