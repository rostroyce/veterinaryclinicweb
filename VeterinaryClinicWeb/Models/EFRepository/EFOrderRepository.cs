﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VeterinaryClinicWeb.Models.Repository;

namespace VeterinaryClinicWeb.Models.EFRepository
{
    public class EFOrderRepository : IOrderRepository
    {
        private VeterinaryClinicDbContext context;

        public EFOrderRepository(VeterinaryClinicDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Order> Orders => context.Orders;

        public bool CreateOrder(Order order)
        {
            try
            {
                context.Orders.Add(order);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public Order GetOrderById(int orderId)
        {
            return context.Orders.FirstOrDefault(order => order.Id == orderId);
        }

        public bool RemoveOrder(Order order)
        {
            try
            {
                context.Orders.Remove(order);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool UpdateOrder(Order order)
        {
            try
            {
                context.Orders.Update(order);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
