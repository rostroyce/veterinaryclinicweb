﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VeterinaryClinicWeb.Models.Repository;

namespace VeterinaryClinicWeb.Models.EFRepository
{
    public class EFDoctorRepository : IDoctorRepository
    {
        private VeterinaryClinicDbContext context;

        public EFDoctorRepository(VeterinaryClinicDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Doctor> Doctors => context.Doctors;

        public bool CreateDoctor(Doctor doctor)
        {
            try
            {
                context.Doctors.Add(doctor);
                context.SaveChanges();
            }
            catch 
            { 
                return false; 
            }

            return true;
        }

        public Doctor GetDoctorById(int doctorId)
        {
            return context.Doctors.FirstOrDefault(doctor => doctor.Id == doctorId);
        }

        public bool RemoveDoctor(Doctor doctor)
        {
            try
            {
                context.Doctors.Remove(doctor);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool UpdateDoctor(Doctor doctor)
        {
            try
            {
                context.Doctors.Update(doctor);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
