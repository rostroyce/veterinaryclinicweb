﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VeterinaryClinicWeb.Models.Repository;

namespace VeterinaryClinicWeb.Models.EFRepository
{
    public class EFServiceRepository : IServiceRepository
    {
        private VeterinaryClinicDbContext context;

        public EFServiceRepository(VeterinaryClinicDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Service> Services => context.Services;

        public bool CreateService(Service service)
        {
            try
            {
                context.Services.Add(service);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public Service GetServiceById(int serviceId)
        {
            return context.Services.FirstOrDefault(service => service.Id == serviceId);
        }

        public bool RemoveService(Service service)
        {
            try
            {
                context.Services.Remove(service);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool UpdateService(Service service)
        {
            try
            {
                context.Services.Update(service);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
