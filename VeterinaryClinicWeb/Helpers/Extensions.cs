using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VeterinaryClinicWeb.Helpers
{
    public static class Extensions
    {
        public static string CapitalizeFirstLetter(this string stringToCapitalize)
        {
            if (stringToCapitalize.Length == 0)
                return "";
            else if (stringToCapitalize.Length == 1)
                return stringToCapitalize.ToUpper();

            return char.ToUpper(stringToCapitalize[0]) + stringToCapitalize.Substring(1).ToLower();
        }
    }
}