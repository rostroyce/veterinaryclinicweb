﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VeterinaryClinicWeb.Helpers
{
    public enum OrderStatus
    {
        IN_PROCESS,
        CHOISING_DATE,
        APPROVED
    }

    public enum ServiceCategory
    {
        INSPECTION,
        SURGERY,
        BEATY
    }
}
