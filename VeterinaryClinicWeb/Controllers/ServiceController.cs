﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VeterinaryClinicWeb.Models;
using VeterinaryClinicWeb.Models.EFRepository;
using Microsoft.EntityFrameworkCore;

namespace VeterinaryClinicWeb.Controllers
{
    public class ServiceController : Controller
    {
        private EFServiceRepository context;
        public ServiceController(VeterinaryClinicDbContext context)
        {
            this.context = new EFServiceRepository(context);
        }

        public IActionResult Index(string category = "no_category")
        {
            if (category == "no_category") 
                return RedirectToAction("Index", "Home");

            IEnumerable<Service> services = context.Services
                .Where(service => service.serviceCategory == category)
                .AsNoTracking()
                .ToList();

            return View(services);
        }
    }
}
