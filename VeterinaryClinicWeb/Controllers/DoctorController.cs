﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using VeterinaryClinicWeb.Models;
using VeterinaryClinicWeb.Models.EFRepository;
using VeterinaryClinicWeb.Models.ViewModels;

namespace VeterinaryClinicWeb.Controllers
{
    public class DoctorController : Controller
    {
        private EFDoctorRepository context;
        public DoctorController(VeterinaryClinicDbContext context)
        {
            this.context = new EFDoctorRepository(context);
        }

        public async Task<IActionResult> Index(int page=1)
        {
            int pageSize = 3;
            int count = await context.Doctors.CountAsync();
            IEnumerable<Doctor> items = await context.Doctors.Skip((page-1) * pageSize).Take(pageSize).ToListAsync();
            
            DoctorIndexViewModel doctorVM = new DoctorIndexViewModel()
            {
               Doctors = items,
               Pagination = new PageViewModel(count, page, pageSize)  
            };
            return View(doctorVM);
        }
    }
}
