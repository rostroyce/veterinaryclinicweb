﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VeterinaryClinicWeb.Models;
using VeterinaryClinicWeb.Models.EFRepository;
using VeterinaryClinicWeb.Models.Validation;
using VeterinaryClinicWeb.Models.ViewModels;

namespace VeterinaryClinicWeb.Controllers
{
    public class OrderController : Controller
    {
        private EFOrderRepository context;
        public OrderController(VeterinaryClinicDbContext context)
        {
            this.context = new EFOrderRepository(context);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Order order)
        {
            bool wasAdded = context.CreateOrder(order);

            if(wasAdded){
                RedirectToAction("Read", "Order", order.Id);
            }
            
            return View();
        }

        [HttpGet]
        public IActionResult Check()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Check(CheckOrderValidation checkOrder)
        {
            if(!ModelState.IsValid)
                return View();

            var order = context
                .Orders
                .Where(o => o.Id == checkOrder.OrderNumber)
                .FirstOrDefault();

            if(order != null && order.Phone.Substring(order.Phone.Length - 4) == checkOrder.PhoneDigits)
            {
                TempData["Permission"] = true;
                return RedirectToAction("Read", "Order", new {id = order.Id});
                //return View("Read", order);
            }

            return RedirectToAction("Index", "Doctor");
        }

        public IActionResult Read(int Id)
        {
            CheckOrderViewModel resultModel = new CheckOrderViewModel()
            {
                order = context.Orders.Where(o => o.Id == Id).FirstOrDefault(),
                HavePermission = (TempData["Permission"] != null) ? Convert.ToBoolean(TempData["Permission"].ToString()) : false
            };

            return View(resultModel);
        }
    }
}
